

function love.load()
 Dragon = love.graphics.newImage("sprites/Dragon.png")
 DragonFire = love.graphics.newImage("sprites/Dragonfire.png")
 
  DragonFirePosY = 0
  DragonFirePosX = 0
  FireCheck = false
 
 
  DragonPosY = 200
  DragonPosX = 2
  DragonHeight = 45
  DragonWidth = 29
   
  mouseX  = love.mouse.getX()
  mouseY = love.mouse.getY()
  love.mouse.setVisible(false)
end

function love.update(dt)

  mouseX  = love.mouse.getX()
  mouseY = love.mouse.getY()
  
  DragonPosY = mouseY
  DragonPosX = mouseX
  
  DragonFirePosY = DragonFirePosY - 15
  
  
  love.graphics.draw(Dragon, DragonPosX, DragonPosY)
  if (FireCheck == false)then
  
  love.graphics.draw(DragonFire, DragonFirePosX, DragonFirePosY)
  end
  if (FireCheck == true and DragonFirePosY<= -170)then
    FireCheck = false
  end 
  



end
function love.draw()
--draw
  love.graphics.setBackgroundColor(255, 255, 255, 255)

  love.graphics.draw(DragonFire, DragonFirePosX, DragonFirePosY)
  love.graphics.draw(Dragon, DragonPosX, DragonPosY)
end
function game_screen()
    
end
function love.keypressed(key)
 
end
function love.mousepressed(x, y, button, istouch)
if (FireCheck == false)then
  

  DragonFirePosX = DragonPosX + 40
  DragonFirePosY = DragonPosY

 FireCheck = true
 end
 
end
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
  x2 < x1+w1 and
  y1 < y2+h2 and
  y2 < y1+h1
end
function pointInRectangle(pointx, pointy, rectx, recty, rectwidth, rectheight)

  wx = rectx + rectwidth
  hx = recty + rectheight
  return pointx > rectx and pointy > recty and pointx < wx and pointy < hx
end
