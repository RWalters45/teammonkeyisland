local Gamestate = require "gamestate"

local menu = Gamestate.new()
local pause = Gamestate.new()
local credits = Gamestate.new()
local level1 = Gamestate.new()
local level1Boss = Gamestate.new()
local level2 = Gamestate.new()
local level2Boss = Gamestate.new()

function love.load()
  -- Load game states
 Gamestate.registerEvents()
 Gamestate.switch(menu)

end

function love.draw()
  Gamestate.draw()
end

function love.update(dt)
  Gamestate.update(dt)
  love.keypressed(key)
end

function menu:init()
  --menuMusic = 
end

function menu:draw()
  love.graphics.print("Main Menu", 10, 50)
end

function menu:update(dt)
  --menuMusic:play()
end

function credits:init()
  
end

function credits:draw()
  love.graphics.print("Credits", 10, 50)
end

function credits:update(dt)
  
end

function level1Boss:init()
  
end

function level1Boss:draw()
  love.graphics.print("Level 1 Boss", 10, 50)
end

function level1Boss:update(dt)
  
end

function level1:init()
  --gameMusic =
  
  background1 = love.graphics.newImage("sprites/backgroundMtn.png")
  background2 = love.graphics.newImage("sprites/backgroundMtn.png")
  frontBackground1 = love.graphics.newImage("sprites/frontBackgroundTrees.png")
  frontBackground2 = love.graphics.newImage("sprites/frontBackgroundTrees.png")
  background1Pos = 0
  background2Pos = 1275
  frontBackground1Pos = 0
  frontBackground2Pos = 3786
  
  DargonFireBurst = {}
  DargonFireBurst.DargonFireBurst1 = love.graphics.newImage("sprites/Dragonfire.png")
  DargonFireBurst.DargonFireBurst2 = love.graphics.newImage("sprites/Dragonfire.png")
  DargonFireBurst.DargonFireBurst3 = love.graphics.newImage("sprites/Dragonfire.png")
  DargonFireBurst.DargonFireBurst4 = love.graphics.newImage("sprites/Dragonfire.png")
  DargonFireBurst.DargonFireBurst5 = love.graphics.newImage("sprites/Dragonfire.png")
  
  DragonFirePosY = 0
  DragonFirePosX = 0
  FireCheck = false
  
  Dragon = love.graphics.newImage("sprites/Dragon.png")
 
  DragonPosY = 200
  DragonPosX = 2
  DragonHeight = 45
  DragonWidth = 29
 
 
   
  mouseX  = love.mouse.getX()
  mouseY = love.mouse.getY()
  
  love.mouse.setVisible(false)
end

function level1:draw()
--draw
love.graphics.draw(background1, background1Pos, 0)
love.graphics.draw(background2, background2Pos, 0)

love.graphics.draw(frontBackground1, frontBackground1Pos, 360)
love.graphics.draw(frontBackground2, frontBackground2Pos, 360)


  love.graphics.setBackgroundColor(255, 255, 255, 255)

  love.graphics.draw(DargonFireBurst.DargonFireBurst1, DragonFirePosX, DragonFirePosY)
  love.graphics.draw(Dragon, DragonPosX, DragonPosY)
  
  
end

function level1:update(dt) 
  --gameMusic:play()
  
  background1Pos = background1Pos - 0.25
  background2Pos = background2Pos - 0.25
  frontBackground1Pos = frontBackground1Pos - 1
  frontBackground2Pos = frontBackground2Pos -1
  
  if background1Pos < -1275 then
    background1Pos = 1275
  end
  if background2Pos < -1275 then
    background2Pos = 1276
  end
  if frontBackground1Pos < -3786 then
    frontBackground1Pos = 3786
  end
  if frontBackground2Pos < -3786 then
    frontBackground2Pos = 3786
  end
  
  mouseX  = love.mouse.getX()
  mouseY = love.mouse.getY()
  
  DragonPosY = mouseY
  DragonPosX = mouseX
  
  DragonFirePosX = DragonFirePosX + 15
  
  
  love.graphics.draw(Dragon, DragonPosX, DragonPosY)
  if (FireCheck == false)then
  
  love.graphics.draw(DargonFireBurst.DargonFireBurst1, DragonFirePosX, DragonFirePosY)
  elseif (FireCheck == true and DragonFirePosX< 170)then

    FireCheck = false
  end 
  
end

function level2:init()

end

function level2:draw()
  love.graphics.print("Level 2", 10, 50)
end

function level2:update(dt) 
  
end

function level2Boss:init()

end

function level2Boss:draw()
  love.graphics.print("Level 2 Boss", 10, 50)
end

function level2Boss:update(dt) 
  
end

function pause:init()
  
end

function pause:draw()
  love.graphics.print("Pause Screen", 10, 50)
end

function pause:update(dt)
  
end

function love.keypressed(key)
  
    if Gamestate.current() == level1 and key == 'p' then
      Gamestate.push(pause)
    end
        
    if Gamestate.current() == menu and key == 'q' then
      Gamestate.push(level1)
    end
        
    if Gamestate.current() == level1 and key == 'p' then
      Gamestate.push(pause)
    end
    
    if Gamestate.current() == pause and key == 'l' then
      Gamestate.push(level1)
    end
    
    if Gamestate.current() == pause and key == 'm' then
      Gamestate.push(menu)
    end
    
    if Gamestate.current() == credits and key == 'm' then
      Gamestate.push(menu)
    end
    
end

function collisions()

end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function love.mousepressed(x, y, button, istouch)
if (FireCheck == false)then
  

  DragonFirePosX = DragonPosX + 40
  DragonFirePosY = DragonPosY 

 
 end
 
end